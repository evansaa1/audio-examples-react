import React, {PureComponent } from 'react';
import Sound from 'react-sound';
import Cmajor from './C.mp3';


class ReactSoundPlayer extends PureComponent{

    constructor(props){
        super(props);
        this.state = {
            playStatus: 'STOPPED'
        }
        this.togglePlay = this.togglePlay.bind(this);
        this.handleAudioFinishedPlaying = this.handleAudioFinishedPlaying.bind(this);
        this.handleAudioPlaying = this.handleAudioPlaying.bind(this);
    }

    togglePlay(){
        const newPlayState = this.state.playStatus === 'PLAYING' ? 'STOPPED' : 'PLAYING';
        this.setState({
            playStatus: newPlayState
        });
    }

    handleAudioPlaying(){
        console.log('Playing');
    }

    handleAudioFinishedPlaying(){
        console.log('Finished playing');
        this.setState({
            playStatus: 'STOPPED'
        });
    }

    render(){
        return <div>
            <Sound
                url={Cmajor}
                playStatus={this.state.playStatus}
                onPlaying={this.handleAudioPlaying}
                onFinishedPlaying={this.handleAudioFinishedPlaying}
            />
            <button onClick={this.togglePlay}>Play / Stop</button>
        </div>
    }
}

export default ReactSoundPlayer;